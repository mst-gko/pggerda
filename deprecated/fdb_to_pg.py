import fdb # pip install fdb
import psycopg2 as pg # pip install psycogp2
import pandas as pd # pip install pandas
from sqlalchemy import create_engine # pip install sqlalchemy
import glob
import errno
import os

def connect_to_db(pghost, pgport, pgdatabase, pguser, pgpassword):
    '''
    Connect to postgres db
    '''
    try:
        con = pg.connect(
            host=pghost,
            port=pgport,
            database=pgdatabase,
            user=pguser,
            password=pgpassword
        )
    except Exception as e:
        print('Unable to connect to database: '+pgdatabase)
        print(e)
    else:
        return con
        print('Connected to database: '+pgdatabase)

def execute_sql(sql_path, sql, con):
	'''
	Desc: Function to execute sql
	param sql_path: path to .sql file
	param sql: inset sql directly in python
	param con: the database connection 
	'''
	if sql_path != '' and sql == '':
		sqlfile = open(sql_path, 'r')
		cur = con.cursor()
		cur.execute(sqlfile.read())  
		con.commit()
		cur.close()
	elif sql_path == '' and sql != '':
		cur = con.cursor()
		cur.execute(sql)  
		con.commit()
		cur.close()
	else:
		print('ERROR: insert string for either sql_path OR sql')

def run_pandas_sql(con_db, sql):
    '''
    Desc: load database query to pandas dataframe
    param con_db: database connection
    param sql: the sql query as one string [str]    
    '''
    try:
        df = pd.read_sql_query(sql,con=con_db)
        return df
    except Exception as e:
        print('SQL could not be executed')
        print(e)
    else:
        print('Sql executed')

def close_db_connection(con):
    '''
    Closes a database connection
    '''
    try:    
        con.close
    except Exception as e:
        print('WARNING: Database could not be closed')
        print(e)
    else:
        print('Database connection closed')

def export_df_to_db(df, pghost, pgport, pguser, pgpassword, pgdatabase, schemaname, tablename):
    '''
    Export pandas df to postgresDB
    '''
    try:
        engine = create_engine(r"postgresql+psycopg2://"+pguser+":"+pgpassword+"@"+pghost+":"+pgport+"/"+pgdatabase)
        df.to_sql(tablename, con=engine, schema=schemaname) 
    except Exception as e:
        print('Table could not be created: '+pgdatabase+'.'+schemaname+'.'+tablename)
        print(e)
    else:  
        print('Table successfully created: '+pgdatabase+'.'+schemaname+'.'+tablename)

def drop_table(con, schemaname, tablename):
    ''' 
    Desc: Drops a table using pandas within psycogp2 database connection
    Dependencies: func execute_sql
    '''
    try:
        execute_sql('','DROP TABLE IF EXISTS '+schemaname+'.'+tablename+' CASCADE;',con)
    except Exception as e: 
        print('Table could not be dropped: '+schemaname+'.'+tablename)
        print(e)
    else:
        print('Table successfully dropped: '+schemaname+'.'+tablename)

# set vars
pghost="10.33.131.50"
pgport="5432"
pgdatabase="pcgerda"
pguser="grukosadmin"
pgpassword="merkel30rblaser"
schemaname = 'dataquality'

databases = [
            r'C:/gitlab/pggerda/data/tem_DK_230718_270718/tem_DK_230718_a.fdb',
            r'C:/gitlab/pggerda/data/mep_dk_130818/mep_dk_130818_a.fdb',
            r'C:/gitlab/pggerda/data/paces_dk_140818/paces_dk_140818.fdb',
            r'C:/gitlab/pggerda/data/skytem_DK_230718_a/skytem_DK_230718_a.fdb'
]

sql_paths = [
            r'C:/gitlab/pggerda/sql/tem',
            r'C:/gitlab/pggerda/sql/mep',
            r'C:/gitlab/pggerda/sql/paces',
            r'C:/gitlab/pggerda/sql/skytem'
]

for database, sql_path in zip(databases, sql_paths):
    con_fdb = fdb.connect(dsn=database, user='sysdba', password='masterkey')
    con_pg = connect_to_db(pghost, pgport, pgdatabase, pguser, pgpassword)
    path = sql_path+r'/*.sql'
    files = glob.glob(path)
    for name in files:
        try:
            with open(name) as f:
                tablename = os.path.splitext(os.path.basename(name))[0]
                query = open(name, 'r').read()
        except IOError as exc:
            if exc.errno != errno.EISDIR:
                raise
         
        if query:
            df = run_pandas_sql(con_fdb, query)
            drop_table(con_pg, schemaname, tablename)
            export_df_to_db(df, pghost, pgport, pguser, pgpassword, pgdatabase, schemaname, tablename)
            sql_geo_permissions = '''
                CREATE EXTENSION IF NOT EXISTS postgis;
                ALTER TABLE {0}.{1} ADD COLUMN geom GEOMETRY(point, 25832);
                UPDATE {0}.{1} tm SET geom = ST_SetSRID(st_makepoint(tm."XUTM",tm."YUTM"),25832);
                CREATE INDEX IF NOT EXISTS dataset_idx ON {0}.{1} USING btree ("DATASET");
                CREATE INDEX IF NOT EXISTS model_idx ON {0}.{1} USING btree ("MODEL");
                CREATE INDEX IF NOT EXISTS model_geom_idx ON {0}.{1} USING gist (geom);
                ALTER TABLE {0}.{1} ADD COLUMN row_id INTEGER;
                with n AS 	(
                    SELECT
                        "index" AS current_id,
                        ROW_NUMBER() OVER () AS row_id
                    FROM {0}.{1}
                )
                    UPDATE {0}.{1}
                    SET row_id = n.row_id
                    FROM n
                    WHERE {0}.{1}."index" = n.current_id;
                GRANT SELECT ON ALL TABLES IN SCHEMA {0} TO grukosreader;
                GRANT USAGE ON SCHEMA {0} TO grukosreader;
            '''.format(schemaname, tablename)
            execute_sql('', sql_geo_permissions, con_pg)
            
            sql_matView = '''
                BEGIN;
                DROP MATERIALIZED VIEW IF EXISTS {0}.mstmvw_{1} CASCADE;

                CREATE MATERIALIZED VIEW {0}.mstmvw_{1} AS (
                    SELECT * 
                    FROM {0}.{1}
                );

                GRANT SELECT ON ALL TABLES IN SCHEMA {0} TO jupiterrole;

                COMMIT;
            '''.format(schemaname, tablename)
            execute_sql('', sql_matView, con_pg)
                    
    close_db_connection(con_fdb)
    close_db_connection(con_pg)

print(os.path.basename(__file__)+r' : Done') 
