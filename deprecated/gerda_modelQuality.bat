@echo off

REM Setting parameters
SET PGCLIENTENCODING=UTF8
SET PGPASSWORD=%DB_ADMIN_PASS%
SET pg_hostname=10.33.131.50
SET pg_username=%DB_ADMIN_USER%
SET pg_port=5432
SET dbname=pcgerda


REM SkyTEM quality
C:\PostgreSQL\10\bin\psql -h %pg_hostname% -p %pg_port% -U %pg_username% -d %dbname% -f C:\gitlab\pggerda\method\skytem\skytem_modeller.sql

REM TEM quality
cmd "/c activate tem_data && python C:\gitlab\pggerda\method\tem\run_tem_quality.py && conda deactivate
C:\PostgreSQL\10\bin\psql -h %pg_hostname% -p %pg_port% -U %pg_username% -d %dbname% -f C:\gitlab\pggerda\method\tem\tem_modeller.sql


pause