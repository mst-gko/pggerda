/*
 * Owner: 	Danish Environmental Protection Agency (EPA) 
 * Descr: 	This sql script updates MST-GKOs pcgerda database
 * 			via GEUS' SQL-gateway where existing tables are updated using 
 * 			select statements executed on a foreign data wrapper. 
 * Author: 	Simon Makwarth <simak@mst.dk>
 * Date c: 	2022-03-30
 * Data m:	2022-03-30
 * License:	GNU GPL v3
 * ERT: 	~120 min
 */

BEGIN;

	CREATE SCHEMA IF NOT EXISTS gerda;

	CREATE SCHEMA IF NOT EXISTS mstgerda;

	CREATE TABLE IF NOT EXISTS mstgerda.mst_exportdate AS 
		(
			SELECT 
				'test_string'::TEXT AS tablename,
				'1900-01-01'::timestamp AS updatetime
			LIMIT 0
		)
	;

	ALTER TABLE mstgerda.mst_exportdate
		DROP CONSTRAINT IF EXISTS mst_exportdate_pkey CASCADE
	; 

	ALTER TABLE mstgerda.mst_exportdate
		ADD PRIMARY KEY (tablename)
	;
  	
COMMIT; 

-- model

BEGIN;

	CREATE TABLE IF NOT EXISTS gerda.model AS 
		(
			SELECT *
			FROM geus_fdw.model
			LIMIT 0
		)
	;

	ALTER TABLE gerda.model
		DROP CONSTRAINT IF EXISTS model_pkey CASCADE
	; 

	ALTER TABLE gerda.model
		DROP CONSTRAINT IF EXISTS xpkmodel CASCADE
	; 

	DROP INDEX IF EXISTS xakmodelident;

	TRUNCATE gerda.model;

	INSERT INTO gerda.model
		(
			SELECT 
				t1.*
			FROM geus_fdw.model t1 
		) 
		ON CONFLICT DO NOTHING
	;

	ALTER TABLE gerda.model
		ADD PRIMARY KEY (model)
	;

	INSERT INTO mstgerda.mst_exportdate AS t1
		(
			tablename,
			updatetime
		)
		VALUES 
			(
				'model',
				now()
			)
		ON CONFLICT (tablename)
			DO UPDATE SET
				updatetime = excluded.updatetime
			WHERE t1.tablename = excluded.tablename
	;

COMMIT;

-- dataset

BEGIN;

	CREATE TABLE IF NOT EXISTS gerda.dataset AS 
		(
			SELECT *
			FROM geus_fdw.dataset
			LIMIT 0
		)
	;

	ALTER TABLE gerda.dataset
		DROP CONSTRAINT IF EXISTS xpkdataset CASCADE
	; 

	ALTER TABLE gerda.dataset
		DROP CONSTRAINT IF EXISTS dataset_pkey CASCADE
	; 

	DROP INDEX IF EXISTS xakdatasetident;

	TRUNCATE gerda.dataset;

	INSERT INTO gerda.dataset
		(
			SELECT 
				t1.*
			FROM geus_fdw.dataset t1 
		) 
		ON CONFLICT DO NOTHING
	;

	ALTER TABLE gerda.dataset
		ADD PRIMARY KEY (dataset)
	;

	INSERT INTO mstgerda.mst_exportdate  AS t1
		(
			tablename,
			updatetime
		)
		VALUES 
			(
				'dataset',
				now()
			)
		ON CONFLICT (tablename)
			DO UPDATE SET
				updatetime = excluded.updatetime
			WHERE t1.tablename = excluded.tablename
	;

COMMIT; 

-- odvpos

BEGIN;

	CREATE TABLE IF NOT EXISTS gerda.odvpos AS 
		(
			SELECT *
			FROM geus_fdw.odvpos
			LIMIT 0
		)
	;

	ALTER TABLE gerda.odvpos
		DROP CONSTRAINT IF EXISTS odvpos_pkey CASCADE
	; 

	TRUNCATE gerda.odvpos;

	INSERT INTO gerda.odvpos
		(
			SELECT 
				t1.*
			FROM geus_fdw.odvpos t1 
		) 
		ON CONFLICT DO NOTHING
	;

	ALTER TABLE gerda.odvpos
		ADD PRIMARY KEY (model, "position")
	;

	INSERT INTO mstgerda.mst_exportdate  AS t1
		(
			tablename,
			updatetime
		)
		VALUES 
			(
				'odvpos',
				now()
			)
		ON CONFLICT (tablename)
			DO UPDATE SET
				updatetime = excluded.updatetime
			WHERE t1.tablename = excluded.tablename
	;

COMMIT; 

-- tdvprpoi

BEGIN;

	CREATE TABLE IF NOT EXISTS gerda.tdvprpoi AS 
		(
			SELECT *
			FROM geus_fdw.tdvprpoi
			LIMIT 0
		)
	;

	ALTER TABLE gerda.tdvprpoi
		DROP CONSTRAINT IF EXISTS tdvprpoi_pkey CASCADE
	; 

	TRUNCATE gerda.tdvprpoi;

	INSERT INTO gerda.tdvprpoi
		(
			SELECT 
				t1.*
			FROM geus_fdw.tdvprpoi t1 
		) 
		ON CONFLICT DO NOTHING
	;

	ALTER TABLE gerda.tdvprpoi
		ADD PRIMARY KEY (model, "point")
	;

	INSERT INTO mstgerda.mst_exportdate  AS t1
		(
			tablename,
			updatetime
		)
		VALUES 
			(
				'tdvprpoi',
				now()
			)
		ON CONFLICT (tablename)
			DO UPDATE SET
				updatetime = excluded.updatetime
			WHERE t1.tablename = excluded.tablename
	;

COMMIT; 

-- odvmodse

BEGIN;

	CREATE TABLE IF NOT EXISTS gerda.odvmodse AS 
		(
			SELECT *
			FROM geus_fdw.odvmodse
			LIMIT 0
		)
	;

	ALTER TABLE gerda.odvmodse
		DROP CONSTRAINT IF EXISTS xpkodvmodse CASCADE
	; 

	ALTER TABLE gerda.odvmodse
		DROP CONSTRAINT IF EXISTS odvmodse_pkey CASCADE
	; 

	ALTER TABLE gerda.odvmodse
		DROP CONSTRAINT IF EXISTS fk_odvmodse_model
	;

	ALTER TABLE gerda.odvmodse
		DROP CONSTRAINT IF EXISTS fk_odvmodse_dataset CASCADE
	;

	TRUNCATE gerda.odvmodse;

	INSERT INTO gerda.odvmodse
		(
			SELECT 
				t1.*
			FROM geus_fdw.odvmodse t1 
		) 
		ON CONFLICT DO NOTHING
	;

	ALTER TABLE gerda.odvmodse
		ADD PRIMARY KEY (model, dataset)
	;

	INSERT INTO mstgerda.mst_exportdate AS t1
		(
			tablename,
			updatetime
		)
		VALUES 
			(
				'odvmodse',
				now()
			)
		ON CONFLICT (tablename)
			DO UPDATE SET
				updatetime = excluded.updatetime
			WHERE t1.tablename = excluded.tablename
	;

COMMIT;
	
-- tdvmodse

BEGIN;

	CREATE TABLE IF NOT EXISTS gerda.tdvmodse AS 
		(
			SELECT *
			FROM geus_fdw.tdvmodse
			LIMIT 0
		)
	;

	ALTER TABLE gerda.tdvmodse
		DROP CONSTRAINT IF EXISTS xpktdvmodse CASCADE
	; 

	ALTER TABLE gerda.tdvmodse
		DROP CONSTRAINT IF EXISTS tdvmodse_pkey CASCADE
	; 

	ALTER TABLE gerda.tdvmodse
		DROP CONSTRAINT IF EXISTS fk_tdvmodse_dataset CASCADE
	;

	ALTER TABLE gerda.tdvmodse
		DROP CONSTRAINT IF EXISTS fk_tdvmodse_model CASCADE 
	;

	TRUNCATE gerda.tdvmodse;

	INSERT INTO gerda.tdvmodse
		(
			SELECT 
				t1.*
			FROM geus_fdw.tdvmodse t1 
		) 
		ON CONFLICT DO NOTHING
	;

	ALTER TABLE gerda.tdvmodse
		ADD PRIMARY KEY (model, dataset)
	;	

	INSERT INTO mstgerda.mst_exportdate AS t1
		(
			tablename,
			updatetime
		)
		VALUES 
			(
				'tdvmodse',
				now()
			)
		ON CONFLICT (tablename)
			DO UPDATE SET
				updatetime = excluded.updatetime
			WHERE t1.tablename = excluded.tablename
	;

COMMIT;
	
-- seicdpposition

BEGIN;

	CREATE TABLE IF NOT EXISTS gerda.seicdpposition AS 
		(
			SELECT *
			FROM geus_fdw.seicdpposition
			LIMIT 0
		)
	;

	ALTER TABLE gerda.seicdpposition
		DROP CONSTRAINT IF EXISTS xpkseicdpposition CASCADE
	; 

	ALTER TABLE gerda.seicdpposition
		DROP CONSTRAINT IF EXISTS seicdpposition_pkey CASCADE
	; 

	ALTER TABLE gerda.seicdpposition
		DROP CONSTRAINT IF EXISTS fk_seicdpposition_dataset CASCADE
	;

	TRUNCATE gerda.seicdpposition;

	INSERT INTO gerda.seicdpposition
		(
			SELECT 
				t1.*
			FROM geus_fdw.seicdpposition t1 
		) 
		ON CONFLICT DO NOTHING
	;

	ALTER TABLE gerda.seicdpposition
		ADD PRIMARY KEY (dataset, "position")
	;

	INSERT INTO mstgerda.mst_exportdate AS t1
		(
			tablename,
			updatetime
		)
		VALUES 
			(
				'seicdpposition',
				now()
			)
		ON CONFLICT (tablename)
			DO UPDATE SET
				updatetime = excluded.updatetime
			WHERE t1.tablename = excluded.tablename
	;

COMMIT;
	
-- loghea

BEGIN;

	CREATE TABLE IF NOT EXISTS gerda.loghea AS 
		(
			SELECT *
			FROM geus_fdw.loghea
			LIMIT 0
		)
	;

	ALTER TABLE gerda.loghea
		DROP CONSTRAINT IF EXISTS xpkloghea CASCADE
	; 

	ALTER TABLE gerda.loghea
		DROP CONSTRAINT IF EXISTS loghea_pkey CASCADE
	; 

	ALTER TABLE gerda.loghea
		DROP CONSTRAINT IF EXISTS fk_loghea_dataset CASCADE 
	;

	TRUNCATE gerda.loghea;

	INSERT INTO gerda.loghea
		(
			SELECT 
				t1.*
			FROM geus_fdw.loghea t1 
		) 
		ON CONFLICT DO NOTHING
	;

	ALTER TABLE gerda.loghea
		ADD PRIMARY KEY (dataset)
	;

	INSERT INTO mstgerda.mst_exportdate AS t1
		(
			tablename,
			updatetime
		)
		VALUES 
			(
				'loghea',
				now()
			)
		ON CONFLICT (tablename)
			DO UPDATE SET
				updatetime = excluded.updatetime
			WHERE t1.tablename = excluded.tablename
	;

COMMIT;

-- odvlayer

BEGIN;

	CREATE TABLE IF NOT EXISTS gerda.odvlayer AS 
		(
			SELECT *
			FROM geus_fdw.odvlayer
			LIMIT 0
		)
	;

	ALTER TABLE gerda.odvlayer
		DROP CONSTRAINT IF EXISTS xpkodvlayer CASCADE
	; 

	ALTER TABLE gerda.odvlayer
		DROP CONSTRAINT IF EXISTS odvlayer_pkey CASCADE
	; 

	ALTER TABLE gerda.odvlayer
		DROP CONSTRAINT IF EXISTS fk_odvlayer_model CASCADE 
	;

	TRUNCATE gerda.odvlayer;

	INSERT INTO gerda.odvlayer
		(
			SELECT 
				t1.*
			FROM geus_fdw.odvlayer t1 
		) 
		ON CONFLICT DO NOTHING
	;

	ALTER TABLE gerda.odvlayer
		ADD PRIMARY KEY (model, "position", layer)
	;

	INSERT INTO mstgerda.mst_exportdate AS t1
		(
			tablename,
			updatetime
		)
		VALUES 
			(
				'odvlayer',
				now()
			)
		ON CONFLICT (tablename)
			DO UPDATE SET
				updatetime = excluded.updatetime
			WHERE t1.tablename = excluded.tablename
	;

COMMIT;

-- odvdoi

BEGIN;

	CREATE TABLE IF NOT EXISTS gerda.odvdoi AS 
		(
			SELECT *
			FROM geus_fdw.odvdoi
			LIMIT 0
		)
	;

	ALTER TABLE gerda.odvdoi
		DROP CONSTRAINT IF EXISTS xpkodvdoi CASCADE
	; 

	ALTER TABLE gerda.odvdoi
		DROP CONSTRAINT IF EXISTS odvdoi_pkey CASCADE
	; 

	ALTER TABLE gerda.odvdoi
		DROP CONSTRAINT IF EXISTS fk_odvdoi_model CASCADE 
	;

	TRUNCATE gerda.odvdoi;

	INSERT INTO gerda.odvdoi
		(
			SELECT 
				t1.*
			FROM geus_fdw.odvdoi t1 
		) 
		ON CONFLICT DO NOTHING
	;

	ALTER TABLE gerda.odvdoi
		ADD PRIMARY KEY (model, "position")
	;

	INSERT INTO mstgerda.mst_exportdate AS t1
		(
			tablename,
			updatetime
		)
		VALUES 
			(
				'odvdoi',
				now()
			)
		ON CONFLICT (tablename)
			DO UPDATE SET
				updatetime = excluded.updatetime
			WHERE t1.tablename = excluded.tablename
	;

COMMIT;


-- odvfwres

BEGIN;

	CREATE TABLE IF NOT EXISTS gerda.odvfwres AS 
		(
			SELECT *
			FROM geus_fdw.odvfwres
			LIMIT 0
		)
	;

	ALTER TABLE gerda.odvfwres
		DROP CONSTRAINT IF EXISTS xpkodvfwres CASCADE
	; 

	ALTER TABLE gerda.odvfwres
		DROP CONSTRAINT IF EXISTS odvfwres_pkey CASCADE
	; 

	ALTER TABLE gerda.odvfwres
		DROP CONSTRAINT IF EXISTS fk_odvfwres_model CASCADE 
	;

	ALTER TABLE gerda.odvfwres
		DROP CONSTRAINT IF EXISTS fk_odvfwres_dataset CASCADE 
	;

	DROP INDEX IF EXISTS idx_odvfwres;

	TRUNCATE gerda.odvfwres;

	INSERT INTO gerda.odvfwres
		(
			SELECT 
				t1.*
			FROM geus_fdw.odvfwres t1 
		) 
		ON CONFLICT DO NOTHING
	;

	ALTER TABLE gerda.odvfwres
		ADD PRIMARY KEY (model, dataset, moposition, daposition, "sequence")
	;
	
	INSERT INTO mstgerda.mst_exportdate AS t1
		(
			tablename,
			updatetime
		)
		VALUES 
			(
				'odvfwres',
				now()
			)
		ON CONFLICT (tablename)
			DO UPDATE SET
				updatetime = excluded.updatetime
			WHERE t1.tablename = excluded.tablename
	;

COMMIT; 

-- logcurve

BEGIN; 

	CREATE TABLE IF NOT EXISTS gerda.logcurve AS 
		(
			SELECT *
			FROM geus_fdw.logcurve
			LIMIT 0
		)
	;

	ALTER TABLE gerda.logcurve
		DROP CONSTRAINT IF EXISTS xpklogcurve CASCADE
	; 

	ALTER TABLE gerda.logcurve
		DROP CONSTRAINT IF EXISTS logcurve_pkey CASCADE
	; 

	ALTER TABLE gerda.logcurve
		DROP CONSTRAINT IF EXISTS fk_logcurve_dataset CASCADE 
	;
	
	ALTER TABLE gerda.logcurve
		DROP CONSTRAINT IF EXISTS fk_logcurve_loghea CASCADE 
	;

	TRUNCATE gerda.logcurve;

	INSERT INTO gerda.logcurve
		(
			SELECT 
				t1.*
			FROM geus_fdw.logcurve t1 
		) 
		ON CONFLICT DO NOTHING
	;

	ALTER TABLE gerda.logcurve
		ADD PRIMARY KEY (dataset, curveno)
	;

	INSERT INTO mstgerda.mst_exportdate AS t1
		(
			tablename,
			updatetime
		)
		VALUES 
			(
				'logcurve',
				now()
			)
		ON CONFLICT (tablename)
			DO UPDATE SET
				updatetime = excluded.updatetime
			WHERE t1.tablename = excluded.tablename
	;

COMMIT;

-- logdata

BEGIN;

	CREATE TABLE IF NOT EXISTS gerda.logdata AS 
		(
			SELECT *
			FROM geus_fdw.logdata
			LIMIT 0
		)
	;

	ALTER TABLE gerda.logdata
		DROP CONSTRAINT IF EXISTS xpklogdata CASCADE
	; 

	ALTER TABLE gerda.logdata
		DROP CONSTRAINT IF EXISTS logdata_pkey CASCADE
	;
	
	ALTER TABLE gerda.logdata
		DROP CONSTRAINT IF EXISTS fk_logdata_dataset CASCADE 
	;

	ALTER TABLE gerda.logdata
		DROP CONSTRAINT IF EXISTS fk_logdata_loghea CASCADE 
	;

	ALTER TABLE gerda.logdata
		DROP CONSTRAINT IF EXISTS fk_logdata_logcurve CASCADE 
	;

	TRUNCATE gerda.logdata;

	INSERT INTO gerda.logdata
		(
			SELECT 
				t1.*
			FROM geus_fdw.logdata t1 
		) 
		ON CONFLICT DO NOTHING
	;

	ALTER TABLE gerda.logdata
		ADD PRIMARY KEY (dataset, curveno, "depth")
	;

	INSERT INTO mstgerda.mst_exportdate AS t1
	(
		tablename,
		updatetime
	)
	VALUES 
	(
		'logdata',
		now()
	)
		ON CONFLICT (tablename)
			DO UPDATE SET
				updatetime = excluded.updatetime
			WHERE t1.tablename = excluded.tablename
	;

COMMIT;

/*
 * ADD FOREIGN KEYS
 */
BEGIN;

	ALTER TABLE gerda.odvmodse
		ADD CONSTRAINT fk_odvmodse_model FOREIGN KEY (model) 
		REFERENCES gerda.model (model)
	;
	
	ALTER TABLE gerda.odvmodse
		ADD CONSTRAINT fk_odvmodse_dataset FOREIGN KEY (dataset) 
		REFERENCES gerda.dataset (dataset)
	;
	
	ALTER TABLE gerda.logdata
		ADD CONSTRAINT fk_logdata_dataset FOREIGN KEY (dataset) 
		REFERENCES gerda.dataset (dataset)
	;
	
	ALTER TABLE gerda.logdata
		ADD CONSTRAINT fk_logdata_loghea FOREIGN KEY (dataset) 
		REFERENCES gerda.loghea (dataset)
	;
	
	ALTER TABLE gerda.logdata
		ADD CONSTRAINT fk_logdata_logcurve FOREIGN KEY (dataset, curveno) 
		REFERENCES gerda.logcurve (dataset, curveno) 
	;
	
	ALTER TABLE gerda.logcurve
		ADD CONSTRAINT fk_logcurve_dataset FOREIGN KEY (dataset) 
		REFERENCES gerda.dataset (dataset)
	;
	
	ALTER TABLE gerda.logcurve
		ADD CONSTRAINT fk_logcurve_loghea FOREIGN KEY (dataset) 
		REFERENCES gerda.loghea (dataset)
	;
	
	ALTER TABLE gerda.odvfwres
		ADD CONSTRAINT fk_odvfwres_model FOREIGN KEY (model) 
		REFERENCES gerda.model (model)
	;
	
	ALTER TABLE gerda.odvfwres
		ADD CONSTRAINT fk_odvfwres_dataset FOREIGN KEY (dataset) 
		REFERENCES gerda.dataset (dataset)
	;
	
	ALTER TABLE gerda.odvdoi
		ADD CONSTRAINT fk_odvdoi_model FOREIGN KEY (model) 
		REFERENCES gerda.model (model)
	;
	
	ALTER TABLE gerda.odvlayer
		ADD CONSTRAINT fk_odvlayer_model FOREIGN KEY (model) 
		REFERENCES gerda.model (model)
	;
	
	ALTER TABLE gerda.loghea
		ADD CONSTRAINT fk_loghea_dataset FOREIGN KEY (dataset) 
		REFERENCES gerda.dataset (dataset)
	;
	
	ALTER TABLE gerda.seicdpposition
		ADD CONSTRAINT fk_seicdpposition_dataset FOREIGN KEY (dataset) 
		REFERENCES gerda.dataset (dataset)
	;

	ALTER TABLE gerda.odvpos
		ADD CONSTRAINT fk_odvpos_model FOREIGN KEY (model) 
		REFERENCES gerda.model (model)
	;

	ALTER TABLE gerda.tdvprpoi
		ADD CONSTRAINT fk_tdvprpoi_model FOREIGN KEY (model) 
		REFERENCES gerda.model (model)
	;
	
	ALTER TABLE gerda.tdvmodse
		ADD CONSTRAINT fk_tdvmodse_model FOREIGN KEY (model) 
		REFERENCES gerda.model (model)
	;
	
	ALTER TABLE gerda.tdvmodse
		ADD CONSTRAINT fk_tdvmodse_dataset FOREIGN KEY (dataset) 
		REFERENCES gerda.dataset (dataset)
	;

	INSERT INTO mstgerda.mst_exportdate AS t1
	(
		tablename,
		updatetime
	)
	VALUES 
	(
		'foreign keys',
		now()
	)
	ON CONFLICT (tablename)
		DO UPDATE SET
			updatetime = excluded.updatetime
		WHERE t1.tablename = excluded.tablename
	;

COMMIT; 