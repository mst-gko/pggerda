CREATE extension IF NOT EXISTS postgis;

DROP TABLE IF EXISTS gerda.geometry;

CREATE TABLE gerda.geometry AS (
	SELECT 
		op.model,
		op."position",
		op.xutm,
		op.yutm 
	FROM gerda.odvpos op 
);

ALTER TABLE gerda.geometry ADD COLUMN geom GEOMETRY(point, 25832);

UPDATE gerda.geometry geo SET geom = ST_SetSRID(st_makepoint(geo."xutm", geo."yutm"),25832);

--CREATE INDEX IF NOT EXISTS model_idx ON gerda.geometry USING btree ("model");

DROP INDEX IF EXISTS model_geom_idx;

CREATE INDEX IF NOT EXISTS model_geom_idx ON gerda.geometry USING gist (geom);

GRANT USAGE ON SCHEMA gerda TO jupiterrole;
GRANT SELECT ON ALL TABLES IN SCHEMA gerda TO jupiterrole;
