select tm."PROJECT", count(tm."RESIDDATA") as total, roe.res_over_1, roe.res_over_1/count(tm."RESIDDATA")::numeric*100 as procent_resOver1
from dataquality.tem_modeller tm
inner join (
	select tm2."PROJECT", count(tm2."RESIDDATA") as res_over_1
	from dataquality.tem_modeller tm2
	where tm2."RESIDDATA" > 1
	group by tm2."PROJECT"
	) roe on tm."PROJECT" = roe."PROJECT"
group by tm."PROJECT", roe.res_over_1
