/*
 * author: 		Simon Makwarth <simak@mst.dk>
 * Maintainer: 	Simon Makwarth <simak@mst.dk>
 * date: 		2021-07-19
 * descr: 		This postgresql sql script drops the schema gerda and pcgerda and 
 * 				is used prior to the weekly pcgerda restore for MST-GKO pcgerda database
 */

DROP SCHEMA IF EXISTS gerda CASCADE;

DROP SCHEMA IF EXISTS pcgerda CASCADE;
CREATE SCHEMA pcgerda;