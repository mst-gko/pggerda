
		SELECT 
			--ROW_NUMBER() over () AS row_num,
			dat.project AS data_ident,
			m.project AS model_ident,
			op.model AS model_geometry,
			oms.model AS model_odvmodse,
			m.model AS model_model,
			op."POSITION", 
			oms.dataset AS dataset_odvmodse,
			dat.dataset AS dataset_dataset,
			CASE 
				WHEN UPPER(oms.DATATYPE) = 'TEM' AND UPPER(oms.datsubtype) IN ('HMTEM1', 'HMTEM2', 'PATEM', 'TEM40', 'TEMCL', 'TEMOFF', 'TEMCO')
					THEN 'tem'
				WHEN UPPER(oms.DATATYPE) = 'TEM' AND oms.datsubtype SIMILAR TO '%skytem%'
					THEN 'skytem'
				WHEN UPPER(oms.DATATYPE) = 'TEM' AND upper(oms.datsubtype) = 'TTEM'
					THEN 'ttem'
				ELSE oms.DATATYPE
				END AS datatype_model,
			op.XUTM,
			op.YUTM 
		FROM ODVPOS op
		FULL OUTER JOIN odvmodse oms ON op.model = oms.model
		FULL OUTER JOIN model m ON op.model = m.model
		FULL OUTER JOIN dataset dat ON oms.dataset = dat.dataset
;

SELECT 
	dat.project AS data_ident,
	m.project AS model_ident,
	op.model AS model_geometry,
	oms.model AS model_odvmodse,
	m.model AS model_model,
	op."POSITION", 
	oms.dataset AS dataset_odvmodse,
	dat.dataset AS dataset_dataset,
	CASE 
		WHEN UPPER(oms.DATATYPE) = 'TEM' AND UPPER(oms.datsubtype) IN ('HMTEM1', 'HMTEM2', 'PATEM', 'TEM40', 'TEMCL', 'TEMOFF', 'TEMCO')
			THEN 'tem'
		WHEN UPPER(oms.DATATYPE) = 'TEM' AND oms.datsubtype SIMILAR TO '%skytem%'
			THEN 'skytem'
		WHEN UPPER(oms.DATATYPE) = 'TEM' AND upper(oms.datsubtype) = 'TTEM'
			THEN 'ttem'
		ELSE oms.DATATYPE
		END AS datatype_model,
	op.XUTM,
	op.YUTM 
FROM ODVPOS op
LEFT JOIN odvmodse oms ON op.model = oms.model
LEFT JOIN model m ON op.model = m.model
LEFT JOIN dataset dat ON oms.dataset = dat.dataset

SELECT *
FROM DATASET d 
	
--	CREATE MATERIALIZED VIEW mstgerda.mstmvw_data_tem AS (
--		SELECT * 
--		FROM mstgerda.mstvw_data_all
--		WHERE datatype_model = 'tem'
--	);
--	
--	CREATE MATERIALIZED VIEW mstgerda.mstmvw_data_skytem AS (
--		SELECT * 
--		FROM mstgerda.mstvw_data_all
--		WHERE datatype_model = 'skytem'
--	);
--
--	CREATE MATERIALIZED VIEW mstgerda.mstmvw_data_ttem AS (
--		SELECT * 
--		FROM mstgerda.mstvw_data_all
--		WHERE datatype_model = 'ttem'
--	);
--	
--	CREATE MATERIALIZED VIEW mstgerda.mstmvw_data_mep AS (
--		SELECT * 
--		FROM mstgerda.mstvw_data_all
--		WHERE datatype_model = 'mep'
--	);
--
--	CREATE MATERIALIZED VIEW mstgerda.mstmvw_data_mep_only2d AS (
--		SELECT 
--			ROW_NUMBER() over () AS row_num,
--			d.project, 
--			d.dataset,
--			d."datatype", 
--			t.model AS tdv_model, 
--			o.model AS odv_model, 
--			tp.point, 
--			ST_SetSRID(st_makepoint(tp."xutm", tp."yutm"),25832) AS geom
--		FROM gerda.dataset d
--		LEFT JOIN gerda.tdvmodse t  ON d.dataset = t.dataset 
--		LEFT JOIN gerda.odvmodse o ON d.dataset = o.dataset
--		LEFT JOIN gerda.tdvprpoi tp ON t.model = tp.model
--		WHERE lower(d."datatype") = 'mep'
--			AND o.model IS NULL
--	);
--	
--	CREATE MATERIALIZED VIEW mstgerda.mstmvw_data_schlumberger AS (
--		SELECT * 
--		FROM mstgerda.mstvw_data_all
--		WHERE datatype_model = 'schlumberger'
--	);
--	
--	CREATE MATERIALIZED VIEW mstgerda.mstmvw_data_paces_pacep AS (
--		SELECT * 
--		FROM mstgerda.mstvw_data_all
--		WHERE datatype_model = 'paces'
--	);
--	
--	
--	CREATE MATERIALIZED VIEW mstgerda.mstmvw_data_isnull AS (
--		SELECT * 
--		FROM mstgerda.mstvw_data_all
--		WHERE datatype_model IS NULL 
--	);
--	
--	CREATE MATERIALIZED VIEW mstgerda.mstmvw_data_log AS (
--		SELECT  
--			ROW_NUMBER() OVER () AS row_num,
--			d.dataset, 
--			d.project, 
--			d.contractor, 
--			d.client, 
--			lh.boreholeno, 
--			b.geom 
--		FROM gerda.dataset d 
--		INNER JOIN gerda.loghea lh on d.dataset = lh.dataset
--		INNER JOIN jupiter_fdw.borehole b on lh.boreholeno = b.boreholeno
--	);
--
--	CREATE MATERIALIZED VIEW mstgerda.mstmvw_data_seismic_cmp AS (
--		SELECT 
--			ROW_NUMBER() OVER () AS row_num,
--			d.dataset, 
--			sp."position", 
--			d.project, 
--			d.contractor, 
--			d.client, 
--			ST_SetSRID(st_makepoint(sp.xutm,sp.yutm),25832) as geom
--		FROM gerda.dataset d 
--		FULL OUTER JOIN gerda.seicdpposition sp ON d.dataset = sp.dataset 
--		WHERE d."datatype" = 'seismic'
--	);
--	
--
--	GRANT SELECT ON ALL TABLES IN SCHEMA mstgerda TO jupiterrole;
--
--COMMIT;