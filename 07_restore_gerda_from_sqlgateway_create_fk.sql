 
/*
 * ADD FOREIGN KEYS
 */
BEGIN;

ALTER TABLE gerda.odvmodse
    ADD CONSTRAINT fk_odvmodse_model FOREIGN KEY (model) 
    REFERENCES gerda.model (model)
;

ALTER TABLE gerda.odvmodse
    ADD CONSTRAINT fk_odvmodse_dataset FOREIGN KEY (dataset) 
    REFERENCES gerda.dataset (dataset)
;

ALTER TABLE gerda.logdata
    ADD CONSTRAINT fk_logdata_dataset FOREIGN KEY (dataset) 
    REFERENCES gerda.dataset (dataset)
;

ALTER TABLE gerda.logdata
    ADD CONSTRAINT fk_logdata_loghea FOREIGN KEY (dataset) 
    REFERENCES gerda.loghea (dataset)
;

ALTER TABLE gerda.logdata
    ADD CONSTRAINT fk_logdata_logcurve FOREIGN KEY (dataset, curveno) 
    REFERENCES gerda.logcurve (dataset, curveno) 
;

ALTER TABLE gerda.logcurve
    ADD CONSTRAINT fk_logcurve_dataset FOREIGN KEY (dataset) 
    REFERENCES gerda.dataset (dataset)
;

ALTER TABLE gerda.logcurve
    ADD CONSTRAINT fk_logcurve_loghea FOREIGN KEY (dataset) 
    REFERENCES gerda.loghea (dataset)
;

ALTER TABLE gerda.odvfwres
    ADD CONSTRAINT fk_odvfwres_model FOREIGN KEY (model) 
    REFERENCES gerda.model (model)
;

ALTER TABLE gerda.odvfwres
    ADD CONSTRAINT fk_odvfwres_dataset FOREIGN KEY (dataset) 
    REFERENCES gerda.dataset (dataset)
;

ALTER TABLE gerda.odvdoi
    ADD CONSTRAINT fk_odvdoi_model FOREIGN KEY (model) 
    REFERENCES gerda.model (model)
;

ALTER TABLE gerda.odvlayer
    ADD CONSTRAINT fk_odvlayer_model FOREIGN KEY (model) 
    REFERENCES gerda.model (model)
;

ALTER TABLE gerda.loghea
    ADD CONSTRAINT fk_loghea_dataset FOREIGN KEY (dataset) 
    REFERENCES gerda.dataset (dataset)
;

ALTER TABLE gerda.seicdpposition
    ADD CONSTRAINT fk_seicdpposition_dataset FOREIGN KEY (dataset) 
    REFERENCES gerda.dataset (dataset)
;

ALTER TABLE gerda.odvpos
    ADD CONSTRAINT fk_odvpos_model FOREIGN KEY (model) 
    REFERENCES gerda.model (model)
;

ALTER TABLE gerda.tdvprpoi
    ADD CONSTRAINT fk_tdvprpoi_model FOREIGN KEY (model) 
    REFERENCES gerda.model (model)
;

ALTER TABLE gerda.tdvmodse
    ADD CONSTRAINT fk_tdvmodse_model FOREIGN KEY (model) 
    REFERENCES gerda.model (model)
;

ALTER TABLE gerda.tdvmodse
    ADD CONSTRAINT fk_tdvmodse_dataset FOREIGN KEY (dataset) 
    REFERENCES gerda.dataset (dataset)
;

INSERT INTO mstgerda.mst_exportdate AS t1
(
    tablename,
    updatetime
)
VALUES 
(
    'foreign keys',
    now()
)
ON CONFLICT (tablename)
    DO UPDATE SET
        updatetime = excluded.updatetime
    WHERE t1.tablename = excluded.tablename
;

COMMIT; 
    