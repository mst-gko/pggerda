import os
from datetime import datetime


def create_sql_string(tables):
    sql = ''

    sql += f'''
/*
 * Owner: 	Danish Environmental Protection Agency (EPA) 
 * Descr: 	This sql script updates MST-GKOs pcgerda database
 * 			via GEUS' SQL-gateway where existing tables are updated using 
 * 			select statements executed on a foreign data wrapper. 
 * Author: 	Simon Makwarth <simak@mst.dk>
 * Date c: 	2022-03-30
 * Data m:	{datetime.today().strftime('%Y-%m-%d')}
 * License:	GNU GPL v3
 * ERT: 	~120 min
 */

BEGIN;

    CREATE SCHEMA IF NOT EXISTS gerda;
    
    CREATE SCHEMA IF NOT EXISTS mstgerda;
    
    CREATE TABLE IF NOT EXISTS mstgerda.mst_exportdate AS 
        (
            SELECT 
                'test_string'::TEXT AS tablename,
                '1900-01-01'::timestamp AS updatetime
            LIMIT 0
        )
    ;
    
    ALTER TABLE mstgerda.mst_exportdate
        DROP CONSTRAINT IF EXISTS mst_exportdate_pkey CASCADE
    ; 
    
    ALTER TABLE mstgerda.mst_exportdate
        ADD PRIMARY KEY (tablename)
    ;

COMMIT; 
    '''

    sql = add_remove_primary_keys_to_sql_string(sql)

    for table in tables:
        sql += f'''
-- {table}

BEGIN;

CREATE TABLE IF NOT EXISTS gerda.{table} AS 
    (
        SELECT *
        FROM geus_fdw.model
        LIMIT 0
    )
;

TRUNCATE gerda.{table};

INSERT INTO gerda.{table}
    (
        SELECT 
            t1.*
        FROM geus_fdw.{table} t1 
    ) 
    ON CONFLICT DO NOTHING
;

INSERT INTO mstgerda.mst_exportdate AS t1
(
    tablename,
    updatetime
)
VALUES 
(
    '{table}',
    now()
)
ON CONFLICT (tablename)
    DO UPDATE SET updatetime = excluded.updatetime
    WHERE t1.tablename = excluded.tablename
;

COMMIT;
        '''

    return sql


def save_sql_to_file(sql_string, output_dir, file_name):
    """
    Save a SQL string to an .sql file and ensure the output directory exists.

    Parameters:
    sql_string (str): The SQL query as a string.
    output_dir (str): The directory where the .sql file should be saved.
    file_name (str): The name of the .sql file (e.g., 'query.sql').
    """
    # Ensure the output directory exists
    os.makedirs(output_dir, exist_ok=True)

    # Create the full path for the .sql file
    file_path = os.path.join(output_dir, file_name)

    # Write the SQL string to the file
    with open(file_path, 'w') as file:
        file.write(sql_string)


def add_remove_primary_keys_to_sql_string(sql):
    sql += f'''
/*
 * Owner: 	Danish Environmental Protection Agency (EPA) 
 * Descr: 	This sql script updates MST-GKOs pcgerda database
 * 			via GEUS' SQL-gateway where existing tables are updated using 
 * 			select statements executed on a foreign data wrapper. 
 * Author: 	Simon Makwarth <simak@mst.dk>
 * Date c: 	2022-03-30
 * Data m:	{datetime.today().strftime('%Y-%m-%d')}
 * License:	GNU GPL v3
 * ERT: 	~1-5 min
 */
 
/*
 * REMOVE PRIMARY KEYS, FOREIGN KEYS AND INDEXES 
 */

BEGIN;

ALTER TABLE gerda.model DROP CONSTRAINT IF EXISTS model_pkey CASCADE; 

ALTER TABLE gerda.model DROP CONSTRAINT IF EXISTS xpkmodel CASCADE; 

DROP INDEX IF EXISTS xakmodelident;

ALTER TABLE gerda.dataset DROP CONSTRAINT IF EXISTS xpkdataset CASCADE; 

ALTER TABLE gerda.dataset DROP CONSTRAINT IF EXISTS dataset_pkey CASCADE; 

DROP INDEX IF EXISTS xakdatasetident;

ALTER TABLE gerda.odvpos DROP CONSTRAINT IF EXISTS odvpos_pkey CASCADE; 

ALTER TABLE gerda.tdvprpoi DROP CONSTRAINT IF EXISTS tdvprpoi_pkey CASCADE; 

ALTER TABLE gerda.odvmodse DROP CONSTRAINT IF EXISTS xpkodvmodse CASCADE; 

ALTER TABLE gerda.odvmodse DROP CONSTRAINT IF EXISTS odvmodse_pkey CASCADE; 

ALTER TABLE gerda.odvmodse DROP CONSTRAINT IF EXISTS fk_odvmodse_model;

ALTER TABLE gerda.odvmodse DROP CONSTRAINT IF EXISTS fk_odvmodse_dataset CASCADE;

ALTER TABLE gerda.tdvmodse DROP CONSTRAINT IF EXISTS xpktdvmodse CASCADE; 

ALTER TABLE gerda.tdvmodse DROP CONSTRAINT IF EXISTS tdvmodse_pkey CASCADE; 

ALTER TABLE gerda.tdvmodse DROP CONSTRAINT IF EXISTS fk_tdvmodse_dataset CASCADE;

ALTER TABLE gerda.tdvmodse DROP CONSTRAINT IF EXISTS fk_tdvmodse_model CASCADE;

ALTER TABLE gerda.seicdpposition DROP CONSTRAINT IF EXISTS xpkseicdpposition CASCADE; 

ALTER TABLE gerda.seicdpposition DROP CONSTRAINT IF EXISTS seicdpposition_pkey CASCADE; 

ALTER TABLE gerda.seicdpposition DROP CONSTRAINT IF EXISTS fk_seicdpposition_dataset CASCADE;

ALTER TABLE gerda.loghea DROP CONSTRAINT IF EXISTS xpkloghea CASCADE; 

ALTER TABLE gerda.loghea DROP CONSTRAINT IF EXISTS loghea_pkey CASCADE; 

ALTER TABLE gerda.loghea DROP CONSTRAINT IF EXISTS fk_loghea_dataset CASCADE;

ALTER TABLE gerda.odvlayer DROP CONSTRAINT IF EXISTS xpkodvlayer CASCADE; 

ALTER TABLE gerda.odvlayer DROP CONSTRAINT IF EXISTS odvlayer_pkey CASCADE; 

ALTER TABLE gerda.odvlayer DROP CONSTRAINT IF EXISTS fk_odvlayer_model CASCADE;

ALTER TABLE gerda.odvdoi DROP CONSTRAINT IF EXISTS xpkodvdoi CASCADE; 

ALTER TABLE gerda.odvdoi DROP CONSTRAINT IF EXISTS odvdoi_pkey CASCADE; 

ALTER TABLE gerda.odvdoi DROP CONSTRAINT IF EXISTS fk_odvdoi_model CASCADE;

ALTER TABLE gerda.odvfwres DROP CONSTRAINT IF EXISTS xpkodvfwres CASCADE; 

ALTER TABLE gerda.odvfwres DROP CONSTRAINT IF EXISTS odvfwres_pkey CASCADE; 

ALTER TABLE gerda.odvfwres DROP CONSTRAINT IF EXISTS fk_odvfwres_model CASCADE;

ALTER TABLE gerda.odvfwres DROP CONSTRAINT IF EXISTS fk_odvfwres_dataset CASCADE;

DROP INDEX IF EXISTS idx_odvfwres;

ALTER TABLE gerda.logcurve DROP CONSTRAINT IF EXISTS xpklogcurve CASCADE; 

ALTER TABLE gerda.logcurve DROP CONSTRAINT IF EXISTS logcurve_pkey CASCADE; 

ALTER TABLE gerda.logcurve DROP CONSTRAINT IF EXISTS fk_logcurve_dataset CASCADE;

ALTER TABLE gerda.logcurve DROP CONSTRAINT IF EXISTS fk_logcurve_loghea CASCADE;

ALTER TABLE gerda.logdata DROP CONSTRAINT IF EXISTS xpklogdata CASCADE; 

ALTER TABLE gerda.logdata DROP CONSTRAINT IF EXISTS logdata_pkey CASCADE;

ALTER TABLE gerda.logdata DROP CONSTRAINT IF EXISTS fk_logdata_dataset CASCADE;

ALTER TABLE gerda.logdata DROP CONSTRAINT IF EXISTS fk_logdata_loghea CASCADE;

ALTER TABLE gerda.logdata DROP CONSTRAINT IF EXISTS fk_logdata_logcurve CASCADE;

INSERT INTO mstgerda.mst_exportdate AS t1
(
    tablename,
    updatetime
)
VALUES 
(
    'remove keys and indexes',
    now()
)
ON CONFLICT (tablename)
    DO UPDATE SET
        updatetime = excluded.updatetime
    WHERE t1.tablename = excluded.tablename
;

COMMIT;
    '''

    return sql


def add_create_primary_keys_to_sql_string(sql):
    sql += '''
/*
 * ADD PRIMARY KEYS 
 */
 
BEGIN;

ALTER TABLE gerda.model ADD PRIMARY KEY (model);

ALTER TABLE gerda.dataset ADD PRIMARY KEY (dataset);

ALTER TABLE gerda.odvpos ADD PRIMARY KEY (model, "position");

ALTER TABLE gerda.tdvprpoi ADD PRIMARY KEY (model, "point");
  
ALTER TABLE gerda.odvmodse ADD PRIMARY KEY (model, dataset);

ALTER TABLE gerda.tdvmodse ADD PRIMARY KEY (model, dataset);

ALTER TABLE gerda.seicdpposition ADD PRIMARY KEY (dataset, "position");

ALTER TABLE gerda.loghea ADD PRIMARY KEY (dataset);

ALTER TABLE gerda.odvlayer ADD PRIMARY KEY (model, "position", layer);

ALTER TABLE gerda.odvdoi ADD PRIMARY KEY (model, "position");

ALTER TABLE gerda.odvfwres ADD PRIMARY KEY (model, dataset, moposition, daposition, "sequence");

ALTER TABLE gerda.logcurve ADD PRIMARY KEY (dataset, curveno);

INSERT INTO mstgerda.mst_exportdate AS t1
(
    tablename,
    updatetime
)
VALUES 
(
    'primary keys',
    now()
)
ON CONFLICT (tablename)
    DO UPDATE SET
        updatetime = excluded.updatetime
    WHERE t1.tablename = excluded.tablename
;

COMMIT;
    '''

    return sql


def add_fk_to_sql_string(sql):
    sql += f''' 
/*
 * ADD FOREIGN KEYS
 */
BEGIN;

ALTER TABLE gerda.odvmodse
    ADD CONSTRAINT fk_odvmodse_model FOREIGN KEY (model) 
    REFERENCES gerda.model (model)
;

ALTER TABLE gerda.odvmodse
    ADD CONSTRAINT fk_odvmodse_dataset FOREIGN KEY (dataset) 
    REFERENCES gerda.dataset (dataset)
;

ALTER TABLE gerda.logdata
    ADD CONSTRAINT fk_logdata_dataset FOREIGN KEY (dataset) 
    REFERENCES gerda.dataset (dataset)
;

ALTER TABLE gerda.logdata
    ADD CONSTRAINT fk_logdata_loghea FOREIGN KEY (dataset) 
    REFERENCES gerda.loghea (dataset)
;

ALTER TABLE gerda.logdata
    ADD CONSTRAINT fk_logdata_logcurve FOREIGN KEY (dataset, curveno) 
    REFERENCES gerda.logcurve (dataset, curveno) 
;

ALTER TABLE gerda.logcurve
    ADD CONSTRAINT fk_logcurve_dataset FOREIGN KEY (dataset) 
    REFERENCES gerda.dataset (dataset)
;

ALTER TABLE gerda.logcurve
    ADD CONSTRAINT fk_logcurve_loghea FOREIGN KEY (dataset) 
    REFERENCES gerda.loghea (dataset)
;

ALTER TABLE gerda.odvfwres
    ADD CONSTRAINT fk_odvfwres_model FOREIGN KEY (model) 
    REFERENCES gerda.model (model)
;

ALTER TABLE gerda.odvfwres
    ADD CONSTRAINT fk_odvfwres_dataset FOREIGN KEY (dataset) 
    REFERENCES gerda.dataset (dataset)
;

ALTER TABLE gerda.odvdoi
    ADD CONSTRAINT fk_odvdoi_model FOREIGN KEY (model) 
    REFERENCES gerda.model (model)
;

ALTER TABLE gerda.odvlayer
    ADD CONSTRAINT fk_odvlayer_model FOREIGN KEY (model) 
    REFERENCES gerda.model (model)
;

ALTER TABLE gerda.loghea
    ADD CONSTRAINT fk_loghea_dataset FOREIGN KEY (dataset) 
    REFERENCES gerda.dataset (dataset)
;

ALTER TABLE gerda.seicdpposition
    ADD CONSTRAINT fk_seicdpposition_dataset FOREIGN KEY (dataset) 
    REFERENCES gerda.dataset (dataset)
;

ALTER TABLE gerda.odvpos
    ADD CONSTRAINT fk_odvpos_model FOREIGN KEY (model) 
    REFERENCES gerda.model (model)
;

ALTER TABLE gerda.tdvprpoi
    ADD CONSTRAINT fk_tdvprpoi_model FOREIGN KEY (model) 
    REFERENCES gerda.model (model)
;

ALTER TABLE gerda.tdvmodse
    ADD CONSTRAINT fk_tdvmodse_model FOREIGN KEY (model) 
    REFERENCES gerda.model (model)
;

ALTER TABLE gerda.tdvmodse
    ADD CONSTRAINT fk_tdvmodse_dataset FOREIGN KEY (dataset) 
    REFERENCES gerda.dataset (dataset)
;

INSERT INTO mstgerda.mst_exportdate AS t1
(
    tablename,
    updatetime
)
VALUES 
(
    'foreign keys',
    now()
)
ON CONFLICT (tablename)
    DO UPDATE SET
        updatetime = excluded.updatetime
    WHERE t1.tablename = excluded.tablename
;

COMMIT; 
    '''

    return sql


output_dir = './'

sql_string = ''
filename = '01_restore_gerda_from_sqlgateway_remove_keys.sql'
sql_string = add_remove_primary_keys_to_sql_string(sql_string)
save_sql_to_file(sql_string, output_dir=output_dir, file_name=filename)

table_names = [
    'model', 'dataset', 'odvpos',
    'tdvprpoi', 'odvmodse', 'tdvmodse',
    'seicdpposition', 'loghea',
    'odvdoi',  'logcurve'
]
filename = '05_restore_gerda_from_sqlgateway_rest.sql'
sql_string = create_sql_string(tables=table_names)

save_sql_to_file(sql_string, output_dir=output_dir, file_name=filename)

table_names = [
    'odvlayer'
]
filename = '04_restore_gerda_from_sqlgateway_odvlayer.sql'
sql_string = create_sql_string(tables=table_names)
save_sql_to_file(sql_string, output_dir=output_dir, file_name=filename)

table_names = [
    'odvfwres'
]
filename = '03_restore_gerda_from_sqlgateway_odvfwres.sql'
sql_string = create_sql_string(tables=table_names)
save_sql_to_file(sql_string, output_dir=output_dir, file_name=filename)

table_names = [
    'logdata'
]
filename = '02_restore_gerda_from_sqlgateway_logdata.sql'
sql_string = create_sql_string(tables=table_names)
save_sql_to_file(sql_string, output_dir=output_dir, file_name=filename)

sql_string = ''
filename = '06_restore_gerda_from_sqlgateway_create_pk.sql'
sql_string = add_create_primary_keys_to_sql_string(sql_string)
save_sql_to_file(sql_string, output_dir=output_dir, file_name=filename)

sql_string = ''
filename = '07_restore_gerda_from_sqlgateway_create_fk.sql'
sql_string = add_fk_to_sql_string(sql_string)
save_sql_to_file(sql_string, output_dir=output_dir, file_name=filename)
