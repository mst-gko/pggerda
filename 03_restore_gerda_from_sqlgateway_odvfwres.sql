
/*
 * Owner: 	Danish Environmental Protection Agency (EPA) 
 * Descr: 	This sql script updates MST-GKOs pcgerda database
 * 			via GEUS' SQL-gateway where existing tables are updated using 
 * 			select statements executed on a foreign data wrapper. 
 * Author: 	Simon Makwarth <simak@mst.dk>
 * Date c: 	2022-03-30
 * Data m:	2025-02-12
 * License:	GNU GPL v3
 * ERT: 	~120 min
 */

BEGIN;

    CREATE SCHEMA IF NOT EXISTS gerda;
    
    CREATE SCHEMA IF NOT EXISTS mstgerda;
    
    CREATE TABLE IF NOT EXISTS mstgerda.mst_exportdate AS 
        (
            SELECT 
                'test_string'::TEXT AS tablename,
                '1900-01-01'::timestamp AS updatetime
            LIMIT 0
        )
    ;
    
    ALTER TABLE mstgerda.mst_exportdate
        DROP CONSTRAINT IF EXISTS mst_exportdate_pkey CASCADE
    ; 
    
    ALTER TABLE mstgerda.mst_exportdate
        ADD PRIMARY KEY (tablename)
    ;

COMMIT; 
    
/*
 * Owner: 	Danish Environmental Protection Agency (EPA) 
 * Descr: 	This sql script updates MST-GKOs pcgerda database
 * 			via GEUS' SQL-gateway where existing tables are updated using 
 * 			select statements executed on a foreign data wrapper. 
 * Author: 	Simon Makwarth <simak@mst.dk>
 * Date c: 	2022-03-30
 * Data m:	2025-02-12
 * License:	GNU GPL v3
 * ERT: 	~1-5 min
 */
 
/*
 * REMOVE PRIMARY KEYS, FOREIGN KEYS AND INDEXES 
 */

BEGIN;

ALTER TABLE gerda.model DROP CONSTRAINT IF EXISTS model_pkey CASCADE; 

ALTER TABLE gerda.model DROP CONSTRAINT IF EXISTS xpkmodel CASCADE; 

DROP INDEX IF EXISTS xakmodelident;

ALTER TABLE gerda.dataset DROP CONSTRAINT IF EXISTS xpkdataset CASCADE; 

ALTER TABLE gerda.dataset DROP CONSTRAINT IF EXISTS dataset_pkey CASCADE; 

DROP INDEX IF EXISTS xakdatasetident;

ALTER TABLE gerda.odvpos DROP CONSTRAINT IF EXISTS odvpos_pkey CASCADE; 

ALTER TABLE gerda.tdvprpoi DROP CONSTRAINT IF EXISTS tdvprpoi_pkey CASCADE; 

ALTER TABLE gerda.odvmodse DROP CONSTRAINT IF EXISTS xpkodvmodse CASCADE; 

ALTER TABLE gerda.odvmodse DROP CONSTRAINT IF EXISTS odvmodse_pkey CASCADE; 

ALTER TABLE gerda.odvmodse DROP CONSTRAINT IF EXISTS fk_odvmodse_model;

ALTER TABLE gerda.odvmodse DROP CONSTRAINT IF EXISTS fk_odvmodse_dataset CASCADE;

ALTER TABLE gerda.tdvmodse DROP CONSTRAINT IF EXISTS xpktdvmodse CASCADE; 

ALTER TABLE gerda.tdvmodse DROP CONSTRAINT IF EXISTS tdvmodse_pkey CASCADE; 

ALTER TABLE gerda.tdvmodse DROP CONSTRAINT IF EXISTS fk_tdvmodse_dataset CASCADE;

ALTER TABLE gerda.tdvmodse DROP CONSTRAINT IF EXISTS fk_tdvmodse_model CASCADE;

ALTER TABLE gerda.seicdpposition DROP CONSTRAINT IF EXISTS xpkseicdpposition CASCADE; 

ALTER TABLE gerda.seicdpposition DROP CONSTRAINT IF EXISTS seicdpposition_pkey CASCADE; 

ALTER TABLE gerda.seicdpposition DROP CONSTRAINT IF EXISTS fk_seicdpposition_dataset CASCADE;

ALTER TABLE gerda.loghea DROP CONSTRAINT IF EXISTS xpkloghea CASCADE; 

ALTER TABLE gerda.loghea DROP CONSTRAINT IF EXISTS loghea_pkey CASCADE; 

ALTER TABLE gerda.loghea DROP CONSTRAINT IF EXISTS fk_loghea_dataset CASCADE;

ALTER TABLE gerda.odvlayer DROP CONSTRAINT IF EXISTS xpkodvlayer CASCADE; 

ALTER TABLE gerda.odvlayer DROP CONSTRAINT IF EXISTS odvlayer_pkey CASCADE; 

ALTER TABLE gerda.odvlayer DROP CONSTRAINT IF EXISTS fk_odvlayer_model CASCADE;

ALTER TABLE gerda.odvdoi DROP CONSTRAINT IF EXISTS xpkodvdoi CASCADE; 

ALTER TABLE gerda.odvdoi DROP CONSTRAINT IF EXISTS odvdoi_pkey CASCADE; 

ALTER TABLE gerda.odvdoi DROP CONSTRAINT IF EXISTS fk_odvdoi_model CASCADE;

ALTER TABLE gerda.odvfwres DROP CONSTRAINT IF EXISTS xpkodvfwres CASCADE; 

ALTER TABLE gerda.odvfwres DROP CONSTRAINT IF EXISTS odvfwres_pkey CASCADE; 

ALTER TABLE gerda.odvfwres DROP CONSTRAINT IF EXISTS fk_odvfwres_model CASCADE;

ALTER TABLE gerda.odvfwres DROP CONSTRAINT IF EXISTS fk_odvfwres_dataset CASCADE;

DROP INDEX IF EXISTS idx_odvfwres;

ALTER TABLE gerda.logcurve DROP CONSTRAINT IF EXISTS xpklogcurve CASCADE; 

ALTER TABLE gerda.logcurve DROP CONSTRAINT IF EXISTS logcurve_pkey CASCADE; 

ALTER TABLE gerda.logcurve DROP CONSTRAINT IF EXISTS fk_logcurve_dataset CASCADE;

ALTER TABLE gerda.logcurve DROP CONSTRAINT IF EXISTS fk_logcurve_loghea CASCADE;

ALTER TABLE gerda.logdata DROP CONSTRAINT IF EXISTS xpklogdata CASCADE; 

ALTER TABLE gerda.logdata DROP CONSTRAINT IF EXISTS logdata_pkey CASCADE;

ALTER TABLE gerda.logdata DROP CONSTRAINT IF EXISTS fk_logdata_dataset CASCADE;

ALTER TABLE gerda.logdata DROP CONSTRAINT IF EXISTS fk_logdata_loghea CASCADE;

ALTER TABLE gerda.logdata DROP CONSTRAINT IF EXISTS fk_logdata_logcurve CASCADE;

INSERT INTO mstgerda.mst_exportdate AS t1
(
    tablename,
    updatetime
)
VALUES 
(
    'remove keys and indexes',
    now()
)
ON CONFLICT (tablename)
    DO UPDATE SET
        updatetime = excluded.updatetime
    WHERE t1.tablename = excluded.tablename
;

COMMIT;
    
-- odvfwres

BEGIN;

CREATE TABLE IF NOT EXISTS gerda.odvfwres AS 
    (
        SELECT *
        FROM geus_fdw.model
        LIMIT 0
    )
;

TRUNCATE gerda.odvfwres;

INSERT INTO gerda.odvfwres
    (
        SELECT 
            t1.*
        FROM geus_fdw.odvfwres t1 
    ) 
    ON CONFLICT DO NOTHING
;

INSERT INTO mstgerda.mst_exportdate AS t1
(
    tablename,
    updatetime
)
VALUES 
(
    'odvfwres',
    now()
)
ON CONFLICT (tablename)
    DO UPDATE SET updatetime = excluded.updatetime
    WHERE t1.tablename = excluded.tablename
;

COMMIT;
        