/*
 * author: 		Simon Makwarth <simak@mst.dk>
 * Maintainer: 	Simon Makwarth <simak@mst.dk>
 * date: 		2021-07-19 
 * Descr: 		This postgresql sql script is used for generating auxiliary functions and
 *  			practical alterations within the pcgerda database after the weekly restore of the pcgerda database. 
 */

-- changing schema name pcgerda to gerda to avoid name coalition between scehma and database
DO
$do$
	BEGIN
		EXECUTE 'COMMENT ON SCHEMA gerda IS '''
		     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
		     || ' simak Schema containing the restored pcgerda database from GEUS'
		     || '''';
	END
$do$
;

-- postgis is the spatial extentison of postgresql handeling geometries 
CREATE EXTENSION IF NOT EXISTS postgis;

-- tablefunc is the extention used for e.g. picking random numbers and crosstab
CREATE EXTENSION IF NOT EXISTS tablefunc;

-- create the schema mstgerda for storing MST-GKO created tables and view derived from geus pcgerda database
CREATE SCHEMA IF NOT EXISTS mstgerda;

DO
$do$
	BEGIN
		EXECUTE 'COMMENT ON SCHEMA mstgerda IS '''
		     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
		     || ' simak Schema containing tables and view created by MST-GKO, which holds the derived data from geus pcgerda database'
		     || '''';
	END
$do$
;

/*
  Make lower letters of column names of all jupiter tables in schema jupiter.
  Change WHERE c.table_schema = 'public' for other schema use
  Rewritten from: http://www.postgresonline.com/journal/archives/141-Lowercasing-table-and-column-names.html
*/
DO
$$
DECLARE
    rec RECORD;
BEGIN
  FOR rec IN
    SELECT (c.table_schema), c.table_name, c.column_name
      FROM information_schema.columns As c
      WHERE c.table_schema = 'gerda'
          AND c.column_name <> lower(c.column_name)
      ORDER BY c.table_schema, c.table_name, c.column_name
    LOOP
      EXECUTE 'ALTER TABLE ' || quote_ident(rec.table_schema) || '.'
      || quote_ident(rec.table_name) || ' RENAME "' || rec.column_name || '" TO ' || quote_ident(lower(rec.column_name)) || ';';
   END LOOP;
END
$$;

-- grant permission to users

GRANT USAGE ON SCHEMA gerda TO jupiterrole;
GRANT SELECT ON ALL TABLES IN SCHEMA gerda TO jupiterrole;
GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA gerda TO jupiterrole;

GRANT USAGE ON SCHEMA mstgerda TO jupiterrole;
GRANT SELECT ON ALL TABLES IN SCHEMA mstgerda TO jupiterrole;
GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA mstgerda TO jupiterrole;

GRANT USAGE ON SCHEMA public TO jupiterrole;
GRANT SELECT ON ALL TABLES IN SCHEMA public TO jupiterrole;
GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA public TO jupiterrole;


-- create index for tables in schema gerda

CREATE INDEX IF NOT EXISTS odvlayer_deptop_idx
	ON gerda.odvlayer 
	USING BTREE (deptop)
;
 
CREATE INDEX IF NOT EXISTS odvlayer_depbottom_idx
	ON gerda.odvlayer 
	USING BTREE (depbottom)
;
