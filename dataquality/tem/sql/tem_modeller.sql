DROP MATERIALIZED VIEW IF EXISTS pcgerda.mstmvw_dataquality_tem;

CREATE MATERIALIZED VIEW pcgerda.mstmvw_dataquality_tem AS ( 
WITH 
	temdata AS (
		SELECT 
			COALESCE(dat.dataset, oms.dataset) AS dataset, 
			oms.model AS model,  
			dat."datatype", 
			th.temsubtype,
			nd.numberofdata,
			CASE
				WHEN tmd.dataset IS NOT NULL
					THEN 'true'
				ELSE 'false'
				END AS missing_datapoints,
			dat.recstartda,
			dat.prostartda,
			dat.insertdate
		FROM pcgerda.dataset dat
		FULL OUTER JOIN pcgerda.odvmodse oms ON dat.dataset = oms.dataset
		LEFT JOIN (
				SELECT 
					tmp.dataset, 
					COUNT(*) AS numberofdata 
				FROM pcgerda.odvfwres tmp 
				GROUP BY tmp.dataset
			) AS nd ON dat.dataset = nd.dataset
		LEFT JOIN dataquality.tem_missing_datapoints tmd on dat.dataset = tmd.dataset
		INNER JOIN pcgerda.temhea th ON dat.dataset = th.dataset
		WHERE upper(dat."datatype") = 'TEM' AND upper(th.temsubtype) IN ('HMTEM1', 'HMTEM2', 'PATEM', 'TEM40', 'TEMCL', 'TEMOFF', 'TEMCO')
	),
	model_test AS (
		SELECT ol.model, 'true' AS possible_copperplate
		FROM pcgerda.odvlayer ol
		WHERE ol.layer = 1
			AND ol.thickness < 2
			AND ol.rho < 10
		GROUP BY ol.model
		ORDER BY ol.model
	)
	SELECT 
		ROW_NUMBER() over () AS row_num,
		m.project,
		m.model,
		td.dataset,
		td."datatype",
		td.temsubtype,
		op."position",
		op.residdata,
		op.residtotal,
		doi.doiupper,
		doi.doilower,
		td.numberofdata,
		td.recstartda,
		td.prostartda,
		td.insertdate,
		COALESCE(mt.possible_copperplate, 'false') AS possible_copperplate,
		td.missing_datapoints,
		geo.geom
	FROM pcgerda.model m
	INNER JOIN temdata td ON m.model = td.model
	INNER JOIN pcgerda.odvpos op ON m.model = op.model
	LEFT JOIN pcgerda.odvdoi doi ON op.model = doi.model AND op."position" = doi."position"
	LEFT JOIN pcgerda.geometry geo ON op.model = geo.model AND op."position" = geo."position"
	LEFT JOIN model_test mt ON m.model = mt.model
	ORDER BY m.PROJECT, op.MODEL, op."position"
);

GRANT SELECT ON ALL TABLES IN SCHEMA pcgerda TO grukosreader;

COMMIT;