# -*- coding: utf-8 -*-
__author__ = 'Anders Damsgaard, Zijad Cosic <zicos@mst.dk>, Simon Makwarth <simak@mst.dk>'
import sys
import os
import re
from functions import replace_whitespaces, correct_dmu_nr


def get_mail_attachments(__outdir__, account_name='MST NJL - GKO Chatterlogger'):
    """
    From a mail account extract the logging csv attachments and save them at selected path
    :param account_name: mail account
    :param __outdir__: directory for the csv output
    :return: log filenames (DMU number)
    """
    import win32com.client
    import datetime

    # Connect to Outlook, which has to be running
    outlook = win32com.client.Dispatch('Outlook.Application').GetNamespace('MAPI')

    for root_folder in outlook.Folders:
        if root_folder.Name == account_name:
            break

    def fetch_attachments_in_messages(messages):
        """
        Function to get the attachment from an outlook message
        :param messages: outlook messages
        :return: log filenames (DMU number)
        """
        n = len(messages)
        print(f' - Total number of messages is {n}')
        i = 0
        j = 0
        message = messages.GetFirst()
        while message:
            date_sent = message.SentOn.strftime("%d%m%y%H%M%S")
            attachments = message.Attachments
            subject = message.Subject
            body_text = message.Body
            msg_from = str(message.Sender).upper()

            if attachments.Count >= 1:
                attachment = attachments.Item(1)
                # fetch dmu number from mail
                if message.Subject.startswith('DMU') or message.Subject.startswith('VS: DMU'):
                    s = re.search(r'DMU\s*\d+', subject)
                    dmu_nr = correct_dmu_nr(s.group(0))
                elif message.Subject.startswith(r'Water level'):
                    s = re.search(r'DMU\s*\d+', body_text)
                    dmu_nr = correct_dmu_nr(s.group(0))
                elif message.Subject.startswith('GKO') or message.Subject.startswith('VS: GKO'):
                    s = re.search(r'GKO\s*\d+', subject)
                    dmu_nr = correct_dmu_nr(s.group(0).replace("GKO", "DMU"))
                elif msg_from.startswith('DMU'):
                    s = re.search(r'DMU\d+', msg_from)
                    dmu_nr = correct_dmu_nr(str(s.group(0)))
                else:
                    dmu_nr = None

                if not dmu_nr:
                    message.move(root_folder.Folders('Indbakke').Folders('import_error'))
                else:
                    filename = replace_whitespaces('{}/{}.{}.{}'.format(__outdir__, dmu_nr, date_sent, attachment))
                    if not os.path.exists(filename):
                        attachment.SaveAsFile(filename)
                        message.move(root_folder.Folders('Indbakke').Folders('imported'))
                        j += 1
                    else:
                        message.move(root_folder.Folders('Indbakke').Folders('csv_duplications'))
            else:
                message.move(root_folder.Folders('Indbakke').Folders('no_attachment'))
            message = messages.GetNext()
            i += 1
            sys.stdout.write('\r - Progress: {:.0f}% (message {})   '
                             .format(float(i/n)*100., i))
            sys.stdout.flush()
        print()
        print('Found {} new messages with attachments'.format(j))

    inbox = root_folder.Folders('Indbakke')
    print('Reading from "{}" in "{}"'.format(inbox.Name, root_folder.Name))
    print('Traversing through mailbox and finding attachments...')
    fetch_attachments_in_messages(inbox.Items)

    # inbox = root_folder.Folders('Indbakke').Folders('imported')
    # print('Reading from "{}" in "{}"'.format(inbox.Name, root_folder.Name))
    # print('Traversing through mailbox and finding attachments...')
    # loggers.extend(fetch_attachments_in_messages(inbox.Items))
