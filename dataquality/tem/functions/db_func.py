# -*- coding: utf-8 -*-
__author__ = 'Simon Makwarth <simak@mst.dk>'


def export_df_to_db(df, pghost, pgport, pguser, pgpassword, pgdatabase, schemaname, tablename):
    """
    Export pandas df to postgresDB
    :param df: pandas dataframe
    :param pghost: database host
    :param pgport: database port
    :param pgdatabase: database name
    :param pguser: database user
    :param pgpassword: database password
    :param schemaname: database schema to export dataframe to
    :param tablename: database table to export dataframe to
    :return:
    """
    from sqlalchemy import create_engine  # pip install sqlalchemy
    try:
        engine = create_engine(
            r"postgresql+psycopg2://" + pguser + ":" + pgpassword + "@" + pghost + ":" + pgport + "/" + pgdatabase)
        df.to_sql(tablename, con=engine, schema=schemaname, if_exists='replace', index=False)
    except Exception as e:
        print('Table could not be edited: ' + pgdatabase + '.' + schemaname + '.' + tablename)
        print(e)


def connect_to_db(pghost, pgport, pgdatabase, pguser, pgpassword):
    """
    connect to postgresql database
    :param pghost: database host
    :param pgport: database port
    :param pgdatabase: database name
    :param pguser: database user
    :param pgpassword: database password
    :return: None
    """
    import psycopg2 as pg
    try:
        con = pg.connect(
            host=pghost,
            port=pgport,
            database=pgdatabase,
            user=pguser,
            password=pgpassword
        )
    except Exception as e:
        print('Unable to connect to database: ' + pgdatabase)
        print(e)
    else:
        return con


def close_db_connection(con):
    """
    Closes a database connection
    :param con: database connection
    :return: none
    """
    try:
        con.close
    except Exception as e:
        print('WARNING: Database could not be closed')
        print(e)


def execute_sql(sql_path, sql, con):
    """
    Execute sql to database connection either from sql or sql-file
    :param sql_path: path to .sql file
    :param sql: inset sql directly in python
    :param con: the psycopg2 connection to a postgres database
    """
    if sql_path != '' and sql == '':
        sql_file = open(sql_path, 'r')
        cur = con.cursor()
        cur.execute(sql_file.read())
        con.commit()
        cur.close()
    elif sql_path == '' and sql != '':
        cur = con.cursor()
        cur.execute(sql)
        con.commit()
        cur.close()
    else:
        print('ERROR: insert string for either sql_path OR sql')


def import_db_to_df(sql, pghost, pgport, pgdatabase, pguser, pgpassword):
    """

    :rtype: object
    """
    import pandas as pd
    con = connect_to_db(pghost, pgport, pgdatabase, pguser, pgpassword)
    try:
        list_existing_csv = pd.read_sql(sql, con)
        return list_existing_csv
    except Exception as e:
        print('sql could not be executed')
        print(e)
    close_db_connection(con)


def load_sql(sql_file):
    fd = open(sql_file, 'r')
    sql = fd.read()
    fd.close()
    return sql