#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from pandas.plotting import register_matplotlib_converters

register_matplotlib_converters()


def plot_level_and_temp(date, level, unit_level, temp, unit_temp, dmu_nr):
    # make plots
    fig = plt.figure(figsize=(10, 7))
    fig.suptitle(dmu_nr, fontsize=16)
    ax0 = plt.subplot(211)
    ax0.plot(date, level)

    ax1 = plt.subplot(212, sharex=ax0)
    ax1.plot(date, temp, '.r')
    ax1.set_ylabel('Temperature [' + unit_temp[0] + ']')
    ax0.set_ylabel('Level [' + unit_level[0] + ']')

    dy = np.max(level) - np.min(level)
    ax0.set_ylim(np.min(level) - dy * 0.1, np.max(level) + dy * 0.1)
    dy = np.max(temp) - np.min(temp)
    ax1.set_ylim(np.min(temp) - dy * 0.1, np.max(temp) + dy * 0.1)

    ax1.xaxis.set_major_locator(matplotlib.dates.YearLocator())
    ax1.xaxis.set_minor_locator(matplotlib.dates.MonthLocator(range(1, 12 + 1)))
    ax1.xaxis.set_major_formatter(matplotlib.dates.DateFormatter('\n%Y'))
    ax1.xaxis.set_minor_formatter(matplotlib.dates.DateFormatter('%b'))

    ax1.tick_params(axis='x', which='major', pad=20, length=6, width=2, rotation=0)
    ax1.tick_params(axis='x', which='minor', rotation=90, length=4, color='grey')
    plt.setp(ax1.get_xticklabels(), ha='center')
    ax0.get_xaxis().set_visible(False)

    # save figures
    plot_path = './plots/'
    if not os.path.exists(plot_path):
        os.makedirs(plot_path)
    plt.savefig(plot_path + '{}.png'.format(dmu_nr))
    plt.savefig(plot_path + '{}.pdf'.format(dmu_nr))

    # close the figure
    plt.close(fig)