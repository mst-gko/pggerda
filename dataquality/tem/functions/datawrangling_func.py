#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'Simon Makwarth <simak@mst.dk>, Anders Damsgaard'


def strip_whitespace(text):
    """
    Strips leading and trailing whitespace from text-string
    :param text: string
    :return string
    """
    try:
        return text.strip()
    except AttributeError:
        return text


def replace_whitespaces(text):
    """
    Replace all whitespace from text-string
    :param text: string
    :return: string
    """
    try:
        return text.replace(" ", "")
    except AttributeError:
        return text


def read_signal(df, signal):
    """
    Takes a pandas dataframe and filter the with regard to param signal
    CAUTION: the two accepted time formats are:
        year-month-day hour:minutes:seconds
        day-month-year hour:minutes:seconds
    :param df: pandas df
        delimiters and comma (,) as decimal separators.
    :param signal: Sensor type, can be 'Temperatur' or 'Niveau'
    :return pandas list of timestamp, the signal value and the unit of the signal
    """
    import pandas as pd

    if len(df['Signal']) < 1:
        print('Warning: Empty CSV file')
        return [0], [0], ''
    else:
        try:
            # new csv files
            df['Date'] = pd.to_datetime(df['Date'], format='%Y-%m-%d %H:%M:%S')
        except:
            try:
                # legacy csv files
                df['Date'] = pd.to_datetime(df['Date'], format='%d-%m-%Y %H:%M:%S')
            except Exception as e:
                print('read_signal error: Date could not be transformed to datatime')
                print(e)
        df_signal = df.loc[df['Signal'].str.strip() == signal]
        return df_signal['Date'].values, \
            df_signal['Value'].values, \
            df_signal['Unit'].values


def get_group_name(df):
    """
    Translate the group names and returns the name in lower case of group in pandas dataframe
    :param df: pandas dataframe
    :return: string of groups
    """
    group_names = df.Signal.unique()
    return group_names


def clean_csv_file(path):
    """
    cleans a csv file by setting decimal-separator to dot (.) and the delimiter to semicolon (;)
    :rtype: text file
    """
    with open(path) as f:
        content = f.read()

    if content.count(';') > 2:
        content = content.replace(',', '.')
        content = content.replace(';', ',')

    content = content.replace(' UTC', '')
    with open(path, "w") as f:
        content = f.write(content)


def correct_dmu_nr(dmu_nr):
    """
    Takes a logger dmu number and transforms it to the form 'DMUXX' (eg. DMU01 instead of DMU1)
    :param dmu_nr: string containing logger dmu number
    :rtype: string
    """
    import re
    if len(replace_whitespaces(dmu_nr)) == 4:
        num = re.search(r'\d', dmu_nr)
        dmu_nr_edit = 'DMU0' + str(num.group(0))
    else:
        dmu_nr_edit = dmu_nr.upper()
    return dmu_nr_edit
