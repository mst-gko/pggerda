"""
    Desc: loop through tem dataset and find soundings with missing data-points
    if times between data-points is not increasing
"""

__author__ = 'Simon Makwarth <simak@mst.dk>'


def find_coupled_tem(pghost, pgport, pgdatabase, pguser, pgpassword):
    from functions.db_func import import_db_to_df, load_sql

    def unique(non_unique_list):
        unique_list = []
        for x in non_unique_list:
            if x not in unique_list:
                unique_list.append(x)
        return unique_list

    # load data from database
    print('Traversing through GERDA database for TEM dataset...')
    sql_file = './sql/get_tem_dbdt.sql'
    sql = load_sql(sql_file)
    df_temwave = import_db_to_df(sql, pghost, pgport, pgdatabase, pguser, pgpassword)

    # group data by dataset-id, position-id and the magnetic moment (segment)
    tem_grouped = df_temwave.groupby(['dataset', 'daposition', 'segment'])
    unique_dataset = tem_grouped['dataset'].unique()
    print(f' - A total of {len(unique_dataset)} tem dataset was found ({len(tem_grouped)} datapoints)')

    # find tem soundings with missing data-points
    print()
    print('Finding tem sounding with missing datapoints...')
    dataset_error = []
    for name, group in tem_grouped:
        group.sort_values(by=['sequence'])
        tem_diff = group['abscival'].diff().diff()

        for i, num in enumerate(tem_diff):
            if num < 0:
                dataset_error.append(name[0])

    tem_coupled = unique(dataset_error)
    print(f' - {len(tem_coupled)} tem dataset id was found missing datapoints in the sounding')
    return tem_coupled

