import os
import matplotlib.pyplot as plt
from functions.db_func import import_db_to_df, load_sql, export_df_to_db
from find_tem_error import find_coupled_tem
import pandas as pd

# set db vars
pghost = "10.33.131.50"
pgport = "5432"
pgdatabase = 'pcgerda'
pguser = os.environ.get('DB_ADMIN_USER')
pgpassword = os.environ.get('DB_ADMIN_PASS')
schemaname = 'dataquality'
tablename = 'tem_missing_datapoints'

error_tem_datasets = pd.DataFrame(find_coupled_tem(pghost, pgport, pgdatabase, pguser, pgpassword), columns =['Dataset'])
export_df_to_db(error_tem_datasets, pghost, pgport, pguser, pgpassword, pgdatabase, schemaname, tablename)

print('done')