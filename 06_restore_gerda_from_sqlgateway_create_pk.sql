
/*
 * ADD PRIMARY KEYS 
 */
 
BEGIN;

ALTER TABLE gerda.model ADD PRIMARY KEY (model);

ALTER TABLE gerda.dataset ADD PRIMARY KEY (dataset);

ALTER TABLE gerda.odvpos ADD PRIMARY KEY (model, "position");

ALTER TABLE gerda.tdvprpoi ADD PRIMARY KEY (model, "point");
  
ALTER TABLE gerda.odvmodse ADD PRIMARY KEY (model, dataset);

ALTER TABLE gerda.tdvmodse ADD PRIMARY KEY (model, dataset);

ALTER TABLE gerda.seicdpposition ADD PRIMARY KEY (dataset, "position");

ALTER TABLE gerda.loghea ADD PRIMARY KEY (dataset);

ALTER TABLE gerda.odvlayer ADD PRIMARY KEY (model, "position", layer);

ALTER TABLE gerda.odvdoi ADD PRIMARY KEY (model, "position");

ALTER TABLE gerda.odvfwres ADD PRIMARY KEY (model, dataset, moposition, daposition, "sequence");

ALTER TABLE gerda.logcurve ADD PRIMARY KEY (dataset, curveno);

INSERT INTO mstgerda.mst_exportdate AS t1
(
    tablename,
    updatetime
)
VALUES 
(
    'primary keys',
    now()
)
ON CONFLICT (tablename)
    DO UPDATE SET
        updatetime = excluded.updatetime
    WHERE t1.tablename = excluded.tablename
;

COMMIT;
    