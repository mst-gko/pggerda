BEGIN;

	DROP TABLE IF EXISTS mstgerda.log_boreholes;

	CREATE TABLE mstgerda.log_boreholes AS 
		(
			SELECT b.boreholeno, b.geom
			FROM jupiter_fdw.borehole b 
			INNER JOIN gerda.loghea lh USING (boreholeno) 
		)
	;

	CREATE INDEX log_boreholes_geom_idx
		ON mstgerda.log_boreholes 
		USING GIST (geom);
		
	CREATE INDEX log_boreholes_boreholeno_idx
		ON mstgerda.log_boreholes 
		USING BTREE (boreholeno);
	
	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE mstgerda.log_boreholes IS '''
			     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
			     || ' simak Table containing borehole DGUnr and geometry of the geophysical logs'
			     || '''';
		END
	$do$
	;

	GRANT SELECT ON ALL TABLES IN SCHEMA mstgerda TO grukosreader;
	
COMMIT;