/*
 * author: 		Simon Makwarth <simak@mst.dk>
 * Maintainer: 	Simon Makwarth <simak@mst.dk>
 * date: 		20210719
 * descr: 		This postgresql sql script creates a table within mstjupiter schema 
 * 				containing the geometry of all 1d and 2d models within pcgerda database
 */

BEGIN;

	CREATE EXTENSION IF NOT EXISTS postgis;
	
	DROP TABLE IF EXISTS mstgerda.model_geometry CASCADE;
	
	CREATE TABLE mstgerda.model_geometry AS 
		( 
			SELECT 
				'1d' AS dimension,
				op.model,
				op."position",
				NULL AS point,
				op.gerdaelevation,
				ST_SetSRID(st_makepoint(op."xutm", op."yutm"), 25832) as geom
			FROM gerda.odvpos op 
			UNION ALL 
			SELECT 
				'2d' AS dimension,
				tp.model,
				NULL AS "position",
				tp.point,
				tp.gerdaelevation, 
				ST_SetSRID(st_makepoint(tp."xutm", tp."yutm"), 25832) as geom
			FROM gerda.tdvprpoi tp
		)
	; 
	
	
	DROP INDEX IF EXISTS model_geom_idx;
	
	CREATE INDEX IF NOT EXISTS model_geom_idx 
		ON mstgerda.model_geometry 
		USING GIST (geom)
	;
	
	CREATE INDEX IF NOT EXISTS model_model_idx 
		ON mstgerda.model_geometry 
		USING BTREE (model)
	;
	
	CREATE INDEX IF NOT EXISTS model_position_idx 
		ON mstgerda.model_geometry 
		USING BTREE ("position")
	;
	
	CREATE INDEX IF NOT EXISTS model_point_idx 
		ON mstgerda.model_geometry 
		USING BTREE (point)
	;
	
	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE mstgerda.model_geometry IS '''
			     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
			     || ' simak Table containing the geometry of all 1d and 2d models within pcgerda database'
			     || '''';
		END
	$do$
	;

COMMIT;

GRANT USAGE ON SCHEMA mstgerda TO jupiterrole;
GRANT SELECT ON ALL TABLES IN SCHEMA mstgerda TO jupiterrole;
