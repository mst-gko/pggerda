/*
 * author: 		Simon Makwarth <simak@mst.dk>
 * Maintainer: 	Simon Makwarth <simak@mst.dk>
 * date: 		20210719
 * descr: 		This postgresql sql script generates information and position for models 
 * 				within the pcgerda database grouped into seperate table by datatype and sub-datetype
 */

-- All models	
BEGIN;

	DROP TABLE IF EXISTS mstgerda.geophysical_model_all CASCADE;
	
	CREATE TABLE mstgerda.geophysical_model_all AS 
		( 
			SELECT DISTINCT ON (m.project, geo.geom)
				ROW_NUMBER() over () AS row_num,
				CASE 
					WHEN lower(oms."datatype") = 'tem' 
						AND COALESCE(lower(oms.datsubtype), 'test') NOT IN ('skytem1', 'skytem2', 'skytemmin1', 'ttem')
						THEN 'tem'
					WHEN lower(oms."datatype") = 'tem' 
						AND lower(oms.datsubtype) IN ('skytem1', 'skytem2', 'skytemmin1')
						THEN 'skytem'
					WHEN lower(oms."datatype") = 'tem' 
						AND lower(oms.datsubtype) = 'ttem'
						THEN 'ttem'
					WHEN tms.model IS NOT NULL
						AND geo.dimension = '2d'
						THEN 'mep'
					ELSE oms."datatype"
					END AS datatype_model,
				d.project AS proj_ident,
				geo.model AS model, 
				geo."position",
				d.dataset AS dataset,
				d.contractor,
				d.client,
				COALESCE(d.recstartda, d.prostartda, d.insertdate, m.insertdate) AS datetime_geus,
				current_date AS datetime_mst,
				geo.dimension,
				geo.geom
			FROM mstgerda.model_geometry geo
			LEFT JOIN gerda.odvmodse oms ON geo.model = oms.model
			LEFT JOIN gerda.tdvmodse tms ON geo.model = tms.model
			LEFT JOIN gerda.model m ON geo.model = m.model
			LEFT JOIN gerda.dataset d ON oms.dataset = d.dataset
		)
	;
	
	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE mstgerda.geophysical_model_all IS '''
			   	|| to_char(LOCALTIMESTAMP, 'YYYYMMDD')
			   	|| ' | User: simak' 
			   	|| ' | Descr: Table containing the placement of all 1d and 2d models within pcgerda database'
			   	|| ' | Repo: https://gitlab.com/mst-gko/pggerda'
			   	|| '''';
		END
	$do$
	;

	CREATE INDEX IF NOT EXISTS geophysical_model_all_geom_idx 
		ON mstgerda.geophysical_model_all 
			USING GIST (geom)
	;
		
	CREATE INDEX IF NOT EXISTS geophysical_model_all_dateset_idx
		ON mstgerda.geophysical_model_all 
			USING BTREE (dataset)
	;

	CREATE INDEX IF NOT EXISTS geophysical_model_all_model_idx
		ON mstgerda.geophysical_model_all 
			USING BTREE (model)
	;

COMMIT;

-- TEM MODELS
BEGIN;
	
	DROP VIEW IF EXISTS mstgerda.model_tem CASCADE;

	CREATE OR REPLACE VIEW mstgerda.model_tem AS 
		(
			SELECT * 
			FROM mstgerda.geophysical_model_all
			WHERE datatype_model = 'tem'
		)
	;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON VIEW mstgerda.model_tem IS '''
			   	|| to_char(LOCALTIMESTAMP, 'YYYYMMDD')
			   	|| ' | simak' 
			   	|| ' | View containing the placement of 1d TEM models within pcgerda database'
			   	|| ' | Repo: https://gitlab.com/mst-gko/pggerda'
			   	|| '''';
		END
	$do$
	;

COMMIT;

-- SKYTEM models
BEGIN; 
	
	DROP VIEW  IF EXISTS mstgerda.model_skytem CASCADE;

	CREATE OR REPLACE VIEW mstgerda.model_skytem AS 
		(
			SELECT * 
			FROM mstgerda.geophysical_model_all
			WHERE datatype_model = 'skytem'
		)
	;



	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON VIEW mstgerda.model_skytem IS '''
			  	|| to_char(LOCALTIMESTAMP, 'YYYYMMDD')
			   	|| ' | User: simak' 
			   	|| ' | Descr: Table containing the placement of 1d SkyTEM models within pcgerda database'
			   	|| ' | Repo: https://gitlab.com/mst-gko/pggerda'
			   	|| '''';
		END
	$do$
	;

COMMIT; 

-- TTEM models
BEGIN; 

	DROP VIEW IF EXISTS mstgerda.model_ttem CASCADE;

	CREATE OR REPLACE VIEW mstgerda.model_ttem AS 
		(
			SELECT * 
			FROM mstgerda.geophysical_model_all
			WHERE datatype_model = 'ttem'
		)
	;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON VIEW mstgerda.model_ttem IS '''
				|| to_char(LOCALTIMESTAMP, 'YYYYMMDD')
			   	|| ' | User: simak' 
				|| ' | Descr: Table containing the placement of 1d tTEM models within pcgerda database'
				|| ' | Repo: https://gitlab.com/mst-gko/pggerda'
			   	|| '''';
		END
	$do$
	;

COMMIT;

-- MEP 1d models
BEGIN;

	DROP VIEW IF EXISTS mstgerda.model_mep_1d CASCADE;

	CREATE OR REPLACE VIEW mstgerda.model_mep_1d AS 
		(
			SELECT * 
			FROM mstgerda.geophysical_model_all
			WHERE datatype_model = 'mep'
				AND dimension = '1d'
		)
	;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON VIEW mstgerda.model_mep_1d IS '''
			     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
			     || ' | User: simak' 
			     || ' | Descr: Table containing the placement of 1d MEP models within pcgerda database'
			     || ' | Repo: https://gitlab.com/mst-gko/pggerda'
			     || '''';
		END
	$do$
	;

COMMIT;

-- MEP 2D models
BEGIN;

	DROP VIEW IF EXISTS mstgerda.model_mep_2d CASCADE;

	CREATE OR REPLACE VIEW mstgerda.model_mep_2d AS 
		(
			SELECT * 
			FROM mstgerda.geophysical_model_all
			WHERE datatype_model = 'mep'
				AND dimension = '2d'
		)
	;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON VIEW mstgerda.model_mep_2d IS '''
			     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
			     || ' | User: simak' 
			     || ' | Descr: Table containing the placement of 2d MEP models within pcgerda database'
			     || 'Repo: https://gitlab.com/mst-gko/pggerda'
			     || '''';
		END
	$do$
	;

COMMIT;
	
-- SCHLUMBERGER models
BEGIN;

	DROP VIEW IF EXISTS mstgerda.model_schlumberger CASCADE;

	CREATE OR REPLACE VIEW mstgerda.model_schlumberger AS 
		(
			SELECT * 
			FROM mstgerda.geophysical_model_all
			WHERE datatype_model = 'schlumberger'
		)
	;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON VIEW mstgerda.model_schlumberger IS '''
			     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
			     || ' User: simak' 
			     || ' | Descr: Table containing the placement of 1d Schlumberger models within pcgerda database'
			     || ' | Repo: https://gitlab.com/mst-gko/pggerda'
			     || '''';
		END
	$do$
	;

COMMIT;
	
-- PACES/PACEP models
BEGIN;

	DROP VIEW IF EXISTS mstgerda.model_paces_pacep CASCADE;

	CREATE OR REPLACE VIEW mstgerda.model_paces_pacep AS 
		(
			SELECT * 
			FROM mstgerda.geophysical_model_all
			WHERE datatype_model = 'paces'
		)
	;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON VIEW mstgerda.model_paces_pacep IS '''
			     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
			     || ' | User: simak' 
			     || ' | Descr: Table containing the placement of 1d paces/pacep models within pcgerda database'
			     || ' | Repo: https://gitlab.com/mst-gko/pggerda'
			     || '''';
		END
	$do$
	;

COMMIT;

-- UNKNOWN models
BEGIN;

	DROP VIEW IF EXISTS mstgerda.model_unknown CASCADE;

	CREATE OR REPLACE VIEW mstgerda.model_unknown AS 
		(
			SELECT * 
			FROM mstgerda.geophysical_model_all
			WHERE datatype_model IS NULL 
		)
	;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON VIEW mstgerda.model_unknown IS '''
			     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
			     || ' | User: simak' 
			     || ' | Descr: Table containing the placement the 1d and 2d model with unknown datatype within pcgerda database'
			     || ' | Repo: https://gitlab.com/mst-gko/pggerda'
			     || '''';
		END
	$do$
	;

COMMIT;


-- Geophysical LOGS points
BEGIN;

	DROP TABLE IF EXISTS mstgerda.model_log CASCADE;

	CREATE TABLE mstgerda.model_log AS 
		(
			SELECT  
				ROW_NUMBER() OVER () AS row_num,
				d.dataset, 
				d.project, 
				lh.boreholeno, 
				d.contractor,
				d.client,
				COALESCE(d.recstartda, d.prostartda, d.insertdate) AS datetime_geus,
				current_date AS datetime_mst,
				b.geom 
			FROM gerda.dataset d 
			INNER JOIN gerda.loghea lh on d.dataset = lh.dataset
			INNER JOIN jupiter_fdw.borehole b on lh.boreholeno = b.boreholeno
		)
	;

	CREATE INDEX IF NOT EXISTS model_log_geom_idx
		ON mstgerda.model_log
			USING GIST (geom)
	;
	
	CREATE INDEX IF NOT EXISTS model_log_idx1
		ON mstgerda.model_log 
			USING BTREE (dataset)
	;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE mstgerda.model_log IS '''
			     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
			     || ' | User: simak' 
			     || ' | Descr: Table containing the placement of geophysical logs within pcgerda database combined with the borehole placement from pcjupiterxl database'
			     || ' | Repo: https://gitlab.com/mst-gko/pggerda'
			     || '''';
		END
	$do$
	;

COMMIT;

-- SEISMIC CMP points
BEGIN;

	DROP TABLE IF EXISTS mstgerda.model_seismic_cmp CASCADE;

	CREATE TABLE mstgerda.model_seismic_cmp AS 
		(
			SELECT 
				ROW_NUMBER() OVER () AS row_num,
				d.project,
				d.dataset, 
				sp."position", 
				d.contractor,
				d.client,
				COALESCE(d.recstartda, d.prostartda, d.insertdate) AS datetime_geus,
				current_date AS datetime_mst,
				ST_SetSRID(st_makepoint(sp.xutm, sp.yutm), 25832) as geom
			FROM gerda.dataset d 
			FULL OUTER JOIN gerda.seicdpposition sp ON d.dataset = sp.dataset 
			WHERE d."datatype" = 'seismic'
		)
	;

	CREATE INDEX IF NOT EXISTS model_seismic_cmp_geom_idx
		ON mstgerda.model_seismic_cmp
			USING GIST (geom)
	;
	
	CREATE INDEX IF NOT EXISTS model_seismic_cmp__dataset_idx
		ON mstgerda.model_seismic_cmp 
			USING BTREE (dataset)
	;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE mstgerda.model_seismic_cmp IS '''
			     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
			     || ' | User: simak' 
			     || ' | Descr: Table containing the placement the common mid point (CMP) of the seismic data within pcgerda database'
			     || ' | Repo: https://gitlab.com/mst-gko/pggerda'
			     || '''';
		END
	$do$
	;

COMMIT;

GRANT USAGE ON SCHEMA gerda TO jupiterrole;
GRANT SELECT ON ALL TABLES IN SCHEMA gerda TO jupiterrole;

GRANT USAGE ON SCHEMA mstgerda TO jupiterrole;
GRANT SELECT ON ALL TABLES IN SCHEMA mstgerda TO jupiterrole;
